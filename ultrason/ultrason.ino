/* HC-SR04 Sensor
   https://www.dealextreme.com/p/hc-sr04-ultrasonic-sensor-distance-measuring-module-133696

   This sketch reads a HC-SR04 ultrasonic rangefinder and returns the
   distance to the closest object in range. To do this, it sends a pulse
   to the sensor to initiate a reading, then listens for a pulse
   to return.  The length of the returning pulse is proportional to
   the distance of the object from the sensor.

   The circuit:
	* VCC connection of the sensor attached to +5V
	* GND connection of the sensor attached to ground
	* TRIG connection of the sensor attached to digital pin 2
	* ECHO connection of the sensor attached to digital pin 4


   Original code for Ping))) example was created by David A. Mellis
   Adapted for HC-SR04 by Tautvidas Sipavicius

   This example code is in the public domain.
 */

#include "Arduino.h"
#include <SoftwareSerial.h>
#include "Comx.h"

#define NPIN 1
const int pinTrig[NPIN] = {2};
const int pinEcho[NPIN] = {4};
long distCm[NPIN];

String order;
char letter;
float args[3];
int argnb;

bool sonar(int pingDevice) {
  long s;

  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(pinTrig[pingDevice], LOW);
  delayMicroseconds(2);
  digitalWrite(pinTrig[pingDevice], HIGH);
  delayMicroseconds(10);
  digitalWrite(pinTrig[pingDevice], LOW);

  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  distCm[pingDevice] = pulseIn(pinEcho[pingDevice], HIGH);

  // Conversion en cm
  distCm[pingDevice] /= 29 / 2;
}

// TODO
// Fonction pour determiner si c'est ok ou pas
bool estOk() {
  return true;
}

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  for (int i=0;i<NPIN;i++)
  {
    pinMode(pinTrig[i], OUTPUT);
    pinMode(pinEcho[i], INPUT);
  }
}

void loop()
{
  // Distances en cm pour chaque module "ping"
  for (int i=0;i<NPIN;i++)
    sonar(i);
  if (Com::readSerial(order)) {
    Com::decryptCommand(order,letter,args,argnb);
    switch (letter){
      case 'B':
          if (estOk())
            Com::writeSerial(StdMsg::ok);
          else
            Com::writeSerial(StdMsg::no);
        break;
    }
  }


  delay(100);
}
