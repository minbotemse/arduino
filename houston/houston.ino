/* code simplifie de commande robot */

// tests des fonctions O,V,T,S et Z realises avec succes
// code pour la commande de servo-moteur (P) manquant, a remplir dans l'interpretation de la commande P
//   Pin non declaree pour servo moteur
// il peut etre necessaire de realiser un recalibrage de la constante K_TOUR (pas pu le faire moi meme manque de batterie)

#include "DualVNH5019MotorShield.h"
#include <SoftwareSerial.h>
#include "Arduino.h"
#include "Comx.h"

// signal = 80 => omega = 360 degree / 5 s
#define T_SAMPLE 50 // resolution en ms
#define T_IDLE 600 // temps apres lequel le robot s'arrete en mode manuel
#define K_ROTATION 5000  // 1 tour en K_ROTATION milliseconde
#define K_TOUR 800  // 1 tour en K_TOUR milliseconde

// limites du signal moteur
#define UBOUND 300  // limite sup
#define LBOUND 50   // limite inf

// variables propres aux ping
#define PIN_NUMBER 3
#define ECHO_TO_CM 0.017f
#define S_WEIGHT 0.7
#define S_TAUX_MAX 0.2

#define PIN_TRIG 10
#define PIN_ECHO0 4
#define PIN_ECHO1 5
#define PIN_ECHO2 6

// port

// variables globales
DualVNH5019MotorShield MotorShield;
int sigVitesse = 0;    // determine la vitesse lineaire du robot
int sigRotation = 0;   // determine la vitesse de rotation du robot
int sigL = 0;          // signal de commande gauche
int sigR = 0;          // signal de commande droit
bool autoMode = true;

unsigned long time;    // date la plus recente d'activite
unsigned long timeRotation;  // date la plus recente de demande d'une rotation par l'utilisateur
unsigned long timeRotationLimit = 0;  // duree au dela de laquelle l'instruction tourner n'a plus effet

unsigned long timeManuel;    // date la plus recente d'activite manuel


// variables relatives aux instructions recues
String order = "";
char letter;
float args[3];
int argnb;

// variables pings
float echoTime[PIN_NUMBER];
float distanceCmWarning[PIN_NUMBER] = {50,50,50};
float echoWarning[PIN_NUMBER];
bool connectedPing[PIN_NUMBER];


void setup() {
  // initialisation des pings
    pinMode(PIN_TRIG,OUTPUT);
    pinMode(PIN_ECHO0,INPUT);
    pinMode(PIN_ECHO1,INPUT);
    pinMode(PIN_ECHO2,INPUT);
  
    for(int i=0;i<PIN_NUMBER;i++) echoTime[i] = 0;
    for(int i=0;i<PIN_NUMBER;i++) echoWarning[i] = distanceCmWarning[i]/ECHO_TO_CM;
    for(int i=0;i<PIN_NUMBER;i++) connectedPing[i] = false;
  
  // initialisation generale
    Serial.begin(9600);  // important pour pouvoir communiquer en bluetooth
    MotorShield.init();  
    time = millis();     // initialise le timer
}

void loop() {
  // decryption de commande
    if(Com::readSerial(order)){
      // en cas de lecture d'un ordre
        Com::decryptCommand(order,letter,args,argnb);
        switch(letter){
        case 'M':
          // mode manuel active
            if(argnb == 0){
                autoMode = false;
                timeManuel = millis();
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Serial.println(argnb);
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'U':
          // planification d'une rotation, 1 argument positif ou negatif
            if(argnb == 0){
                autoMode = true;
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'O':
          // planification d'une rotation, 1 argument positif ou negatif
            if(argnb == 1){
                setRotation(args[0]);
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'V':
          // changement de vitesse, 1 argument positif ou negatif
            if(argnb == 1){
                setVitesse(args[0]);
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'T':
          // planification d'une tornade, 1 argument positif ou negatif
            if(argnb == 1){
                setVitesse(0);    // robot en mode sur-place
                setTour(args[0]);
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'S':
          // arret du robot
            resetSignal();
            Com::writeBluetooth(StdMsg::ok);
            break;
        case 'P':
          // fermeture ou ouverture des pinces
            if(argnb == 1){
                // active pince ou pas
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'D':
          // mode joystick
            if(argnb == 2){
                // cos et sin * 100
                sigL = args[1] + args[0];
                sigR = args[1] - args[0];
                timeManuel = millis();
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        case 'Z':
          // test pour calibration rotation
            if(argnb == 2){
                testRotation(args[0],args[1]);
                Com::writeBluetooth(StdMsg::ok);
            } else {
                Com::writeBluetooth(StdMsg::no);
            }
            break;
        }
    }
    
    // debut du process    
    if((millis() - time) > T_SAMPLE){ 
        time = millis();  // maj du timer
        
      // detection des obstacles
        connectedPing[0] = sonar(PIN_TRIG,PIN_ECHO0,echoTime[0]);
        connectedPing[1] = sonar(PIN_TRIG,PIN_ECHO1,echoTime[1]);
        connectedPing[2] = sonar(PIN_TRIG,PIN_ECHO2,echoTime[2]);
        
      // debug
        //Serial.println("echo 0 : "+String(echoTime[0]*ECHO_TO_CM));
        //Serial.println("echo 1 : "+String(echoTime[1]*ECHO_TO_CM));
        //Serial.println("echo 2 : "+String(echoTime[2]*ECHO_TO_CM));
        
      // envoie de message si besoin
        for(int i=0;i<PIN_NUMBER;i++){
            if(checkObstacle(connectedPing[i],echoTime[i],echoWarning[i]))
                Com::writeBluetooth(StdMsg::detectObstacle(-PI/3 + i * PI/3));
        }
        
      // debug
        //Serial.println("vitesse et rotation : " + String(sigVitesse) + " - " + String(sigRotation));
        //Serial.println("signal moteur LR : " + String(sigL) + " - " + String(sigR));
        //Serial.println("time rot et limit : " + String(timeRotation) + " - " + String(timeRotationLimit));
        //Serial.println("time manuel et time : " + String(timeManuel) + " - " + String(time));
        //Serial.println("time : " + String(time));
        
      // gestion de l'action
        //time = millis();
        if(autoMode){
            if((time - timeRotation) > timeRotationLimit){
              // cas normal, le timer indique que le temps de la rotation est depasse
                sigL = sigVitesse;
                sigR = sigVitesse;
            } else {
              // cas d'une rotation ou d'une tornade
                sigL = sigVitesse - sigRotation;
                sigR = sigVitesse + sigRotation;
            }
        } else {
            if((time - timeManuel) > T_IDLE){
              // cas normal, le timer indique que la portee de l'action est depasse
                sigL = 0;
                sigR = 0;
            }
        }
        
      // maj des commandes moteur
        motor(sigL,sigR);
        
      // fin du process
    }
}

// fonctions de commande du robot
void testRotation(int timeSecond,int sig){
  // fait tourner le robot pendant timeSecond avec le signal sig pour calibration
    sigRotation = sig;
    timeRotation = millis();
    timeRotationLimit = timeSecond * 1000;
}
void setRotation(float angle){
  // planifie une rotation d'ange <angle>
    sigRotation = angle>0?80:-80;
    timeRotation = millis();
    timeRotationLimit = round((float) abs(angle) / 360 * K_ROTATION);
}

void setTour(int tour){
  // planifie une rotation de <tour> tour a toute allure, aussi appele tornade
    sigRotation = tour>0?300:-300;
    timeRotation = millis();
    timeRotationLimit = round((float) abs(tour) / 360 * K_TOUR);
}

void setVitesse(int vitesse){
  // change la vitesse du robot en evitant les zones de commande
  //ou le signal n'est pas suffisant pour avancer
  //ou le signal est trop important
    if(vitesse > UBOUND) sigVitesse = UBOUND;
    else if (vitesse > LBOUND) sigVitesse = vitesse;
    else if (vitesse > 0) sigVitesse = LBOUND;
    else if (vitesse == 0) sigVitesse = 0;
    else if (vitesse > -LBOUND) sigVitesse = -LBOUND;
    else if (vitesse > -UBOUND) sigVitesse = vitesse;
    else sigVitesse = -UBOUND;
}

void resetSignal(){
  // stop le robot
    sigVitesse = 0;
    sigRotation = 0;
}

void motor(int sigL, int sigR){
  // commande le shield arduino
    MotorShield.setM2Speed(sigL);
    MotorShield.setM1Speed(sigR);
}


// fonctions de commande des pings
bool sonar(int pinTrig, int pinEcho, float &inValue){
    float delta;
  // mise au repose de la pin de lecture
    digitalWrite(pinEcho,LOW);
  // declenche l'emission d'une salve ultrason par un ping HCSR04
    digitalWrite(pinTrig,HIGH);
    delayMicroseconds(10);
    digitalWrite(pinTrig,LOW);
  // lecture du temps d'echo avec detection d'une non connection (return 0)
    delta = pulseIn(pinEcho,HIGH,17000);
    if(delta != 0){
        delta -= inValue;   // calcul de la variation entre le signal precedent et courant avant lissage
      // limitation des grandes variations
        if(inValue != 0){
          delta = min(delta,abs(inValue)*S_TAUX_MAX);
          delta = max(delta,-abs(inValue)*S_TAUX_MAX);
        }
      // filtrage passe-bas
        inValue += S_WEIGHT * delta;
        return true;
    } else {
      //ping non connecté
        return false;
    }
}

bool checkObstacle(bool isConnected, float echo, float echoMax){
  return (isConnected && (echo < echoMax));
}
