#ifndef FIFO_H_
#define FIFO_H_

template <typename T>
class Link {
  public :
    Link<T> * next;
    Link<T> * prev;
    T value;
    
    Link(T val, Link<T> *n, Link<T> *p) {
      value = val;
      next = n;
      prev = p;
    }
    
    Link(T val) {
      value = val;
      next = NULL;
      prev = NULL;  
    }
};

template <typename T>
class FIFO {
  /* FIFO task list */
  private :
    Link<T> * startLink;
    Link<T> * endLink;
  
  public :
    FIFO() {
      startLink = NULL;
      endLink = NULL;
    }
    
    void add(T s) {
      Link<T> *link = new Link<T>(s, NULL, endLink);
      //Serial.println("Ajout de : " + s);
      
      if(startLink == NULL) {
        startLink = link;
        endLink = link;
      }
      else if(endLink != NULL) {
          endLink->next = link;
          endLink = link;
        }
        
      //Serial.print("qui est suivi de : ");
      //Serial.println((long)link->next);
    }
    
    bool isEmpty () {
      return startLink == NULL;
    }
    
    /**
     * Permet de prendre le premier de la liste tout en l'enlevant
     */
    T pull () {
        T s = startLink->value;
        Link<T> * second = startLink->next;
        //Serial.print("Pull de : " + s + " (suivant : ");
        //Serial.print((long)second);
        //Serial.println(")");
        
        if(second != NULL)
          second->prev = NULL;
        delete startLink;
        startLink = second;
        
        return s;
    }
    
    void clearList() {
       while(!isEmpty())
        pull(); 
    }
};

#endif /* FIFO_H_ */



