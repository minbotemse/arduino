#include <SoftwareSerial.h>
#include "Arduino.h"

#ifndef ILIST_H_
#define ILIST_H_

class Link {
  public :
    Link * next;
    Link * prev;
    String value;
    
    Link(String val, Link *n, Link *p) {
      value = val;
      next = n;
      prev = p;
    }
    
    Link(String val) {
      value = val;
      next = NULL;
      prev = NULL;  
    }
};

class InstList {
  /* FIFO task list */
  private :
    Link * startLink;
    Link * endLink;
  
  public :
    InstList() {
      startLink = NULL;
      endLink = NULL;
    }
    
    void add(String s) {
      Link *link = new Link(s, NULL, endLink);
      //Serial.println("Ajout de : " + s);
      
      if(startLink == NULL) {
        startLink = link;
        endLink = link;
      }
      else if(endLink != NULL) {
          endLink->next = link;
          endLink = link;
        }
        
      //Serial.print("qui est suivi de : ");
      //Serial.println((long)link->next);
    }
    
    bool isEmpty () {
      return startLink == NULL;
    }
    
    /**
     * Permet de prendre le premier de la liste tout en l'enlevant
     */
    String pull () {
        String s = startLink->value;
        Link * second = startLink->next;
        //Serial.print("Pull de : " + s + " (suivant : ");
        //Serial.print((long)second);
        //Serial.println(")");
        
        if(second != NULL)
          second->prev = NULL;
        delete startLink;
        startLink = second;
        
        return s;
    }
    
    void clearList() {
       while(!isEmpty())
        pull(); 
    }
};

#endif /* ILIST_H_ */



