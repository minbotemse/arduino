/* SOURCE : MASTER PIC + SERVO */

#include <SoftwareSerial.h>
#include "Arduino.h"
#include "Comx.h"
#include "FIFO.h"


#define servoAction(pin,boole) analogWrite(pin,boole?255:0);

#define SERVO_T 1000

//Bluetooth on 0,1
#define PIN_ULTRASON_RX 2
#define PIN_ULTRASON_TX 3
#define PIN_MOTEUR_RX 4
#define PIN_MOTEUR_TX 5

#define PIN_SERVO_BAR 10
#define PIN_SERVO_GOB 11
#define PIN_SERVO_POP 12
#define PIN_SERVO_CYL 13

// structure
enum State{LOAD,SEND,RECEIVE};
enum DeviceType{MOTOR, PING, NONE};

struct MailBox{
    DeviceType id;
    bool couldAnswer;
};

SoftwareSerial ultrasonSerial(PIN_ULTRASON_RX, PIN_ULTRASON_TX);
SoftwareSerial motorSerial(PIN_MOTEUR_RX, PIN_MOTEUR_TX);

MailBox comPort;
FIFO<String> instructions;
String curInst;

String order = "";
char letter;
float args[3];
int argnb;

String answer;


bool autoMode = false;
bool run = false;
  
void setup() {
  pinMode(PIN_ULTRASON_RX,INPUT);
  pinMode(PIN_ULTRASON_TX,OUTPUT);
  pinMode(PIN_MOTEUR_RX,INPUT);
  pinMode(PIN_MOTEUR_TX,OUTPUT);  
  
  pinMode(PIN_SERVO_BAR,OUTPUT);
  pinMode(PIN_SERVO_GOB,OUTPUT);
  pinMode(PIN_SERVO_POP,OUTPUT);
  pinMode(PIN_SERVO_CYL,OUTPUT);
  
  MailBox comPort = {NONE,false};
  Serial.begin(9600);
  motorSerial.begin(9600);
  ultrasonSerial.begin(9600);
}

void loop() {
    // acquisition des ordres
    if(!run && Com::readSerial(order)){
       if(autoMode){
           if(order=="END!"){
               if(!instructions.isEmpty()){
                   curInst = instructions.pull();
                   Com::writeSerial(StdMsg::startAction(curInst));
                   run = true;
               }
           } else instructions.add(order);
       } else {
           curInst = order;
           Com::writeSerial(StdMsg::startAction(curInst));
           run = true;
       }
    } 
    // traitement des ordres
    if(run) {
        // reception des colis en attentes
        if(comPort.couldAnswer){
            switch(comPort.id){
            case MOTOR:
                if(Com::readSerial(answer,motorSerial)) comPort.couldAnswer = false;
                break;
            case PING:
                if(Com::readSerial(answer,ultrasonSerial)) comPort.couldAnswer = false;
                break;
            default:
                answer = "";
            }
            if(comPort.id!=NONE && !comPort.couldAnswer){
                comPort.id = NONE;   
                if(answer == StdMsg::ok){
                    Com::writeSerial(StdMsg::endAction(curInst));
                    if(instructions.isEmpty()){
                        if(autoMode) Com::writeSerial(StdMsg::ok);
                        run = false;
                    } else {
                        curInst = instructions.pull();
                        Com::writeSerial(StdMsg::startAction(curInst));
                    }
                } else {
                    Com::writeSerial(answer);
                    instructions.clearList();
                    run = false;        
                }
            }
        } else {
            // router part
            letter = curInst.charAt(0);
            switch(letter){
            case 'C':
            case 'O':
            case 'A':
                comPort.id = MOTOR;
                comPort.couldAnswer = true;
                Com::writeSerial(curInst,motorSerial);
                break;
            case 'M':
                autoMode = false;
                Com::writeSerial(StdMsg::ok);
                break;
            case 'W':
                autoMode = true;
                Com::writeSerial(StdMsg::ok);
                break;
            default:
                if(instructions.isEmpty()){
                    if(autoMode) Com::writeSerial(StdMsg::ok);
                    run = false;
                } else {
                    curInst = instructions.pull();
                    Com::writeSerial(StdMsg::startAction(curInst));
                }
            }
        }
    }
}
          
      
 /*     
  if(autoFlag){
    int i = 0;
    while((i < INST_NB) && MProtocol::receiveTrameFromMaster(order)){
      action.add(order);
      i++;
    }
  } else {
    MProtocol::receiveTrameFromMaster(order);
    action.add(order);
  }
      
  // boucle d'effection des ordres
  instToDo = action.getSize();
  if(instToDo>0){
    while(instToDo>0){
      order = action.getLast();
      instToDo--;
      // traitment de l'action
      MProtocol::decryptCommand(order,letter,args,argnb);
      switch(letter){
        // cas d'un servo
        case 'Y':
            MProtocol::sendTrameToMaster(StdMessage::startAction(order));
            if(argnb != 1){
                MProtocol::sendTrameToMaster(StdMessage::pb);
                instToDo = -1;
            } else {
                servoAction(PIN_SERVO_CYL,args[0]);
                delay(SERVO_T);
                MProtocol::sendTrameToMaster(StdMessage::endAction(order));
            }
            break;
        case 'G':
            MProtocol::sendTrameToMaster(StdMessage::startAction(order));
            if(argnb != 1){
                MProtocol::sendTrameToMaster(StdMessage::pb);
                instToDo = -1;
            } else {
                servoAction(PIN_SERVO_GOB,args[0]);
                delay(SERVO_T);
                MProtocol::sendTrameToMaster(StdMessage::endAction(order));
            }
            break;
        case 'P':
            MProtocol::sendTrameToMaster(StdMessage::startAction(order));
            if(argnb != 1){
                MProtocol::sendTrameToMaster(StdMessage::pb);
                instToDo = -1;
            } else {
                servoAction(PIN_SERVO_POP,args[0]);
                delay(SERVO_T);
                MProtocol::sendTrameToMaster(StdMessage::endAction(order));
            }
            break;
        case 'L':
            MProtocol::sendTrameToMaster(StdMessage::startAction(order));
            if(argnb != 1){
                MProtocol::sendTrameToMaster(StdMessage::pb);
                instToDo = -1;
            } else {
                servoAction(PIN_SERVO_BAR,args[0]);
                delay(SERVO_T);
                MProtocol::sendTrameToMaster(StdMessage::endAction(order));
            }
            break;
      
        // cas des moteurs
        case 'C':
        case 'O':
        case 'A':      
        case 'D':
            if(letter == 'D' && !autoFlag){
              MProtocol::sendTrameToSlave(order,motor);
            } else {
              if(!MProtocol::transferTrame(order,motor)){
                instToDo = -1;
              }
            }
            break;          
      
        // cas des pings
        case 'T':
        case 'B':
            if(!MProtocol::transferTrame(order,ultrason)){
              instToDo = -1;
            }
            break;
        case 'M':
            MProtocol::sendTrameToSlave("M!",motor);
            autoFlag = false;
            break;
        case 'U':
            MProtocol::sendTrameToSlave("U!",motor);
            autoFlag = true;
            break;
        default :
            continue;
      }
    }
    if(instToDo == 0) MProtocol::sendTrameToMaster(StdMessage::ok);
    action.clearList();
  }
}
*/
