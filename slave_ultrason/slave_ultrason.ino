/*  mesure de distance ultrason HC-SR04  */
/*  lissage exponentielle et limitation du taux de variation  */

#include "Arduino.h"
#include <SoftwareSerial.h>
#include "Comx.h"

/* feu avant */
#define PIN_NUMBER 3

#define PIN_TRIG0 4
#define PIN_TRIG1 4
#define PIN_TRIG2 4
#define PIN_ECHO0 7
#define PIN_ECHO1 8
#define PIN_ECHO2 9

/* feu de recul */
#define PINBACK_TRIG 3
#define PINBACK_ECHO 10

/* warning */
#define PIN_OBSTACLE 13

#define readEcho(pin) pulseIn(pin,HIGH,17000)

#define ECHO_C 0.017f
#define S_WEIGHT 0.7
#define S_TAUX_MAX 0.2

#define SEND_LAPSETIME 10 //ms

float echoTime[PIN_NUMBER];
float distanceCmWarning[PIN_NUMBER] = {20,30,30};
float echoWarning[PIN_NUMBER];
bool connectedPing[PIN_NUMBER];

float echoWarningBack = 5 / ECHO_C;
bool connectedPingBack = true;
float echoTimeBack = 0;

String order;
char letter;
float args[3];
int argnb;
unsigned long t;

  
void setup() {
  pinMode(PIN_TRIG0,OUTPUT);
  pinMode(PIN_ECHO0,INPUT);
  pinMode(PIN_TRIG1,OUTPUT);
  pinMode(PIN_ECHO1,INPUT);
  pinMode(PIN_TRIG2,OUTPUT);
  pinMode(PIN_ECHO2,INPUT);
  
  pinMode(PINBACK_TRIG,OUTPUT);
  pinMode(PINBACK_ECHO,INPUT);
  
  pinMode(PIN_OBSTACLE,OUTPUT);
  
  digitalWrite(PIN_TRIG0,LOW);
  digitalWrite(PIN_TRIG1,LOW);
  digitalWrite(PIN_TRIG2,LOW);
  digitalWrite(PINBACK_TRIG,LOW);
  digitalWrite(PIN_OBSTACLE,LOW);
  
  for(int i=0;i<PIN_NUMBER;i++) echoTime[i] = 0;
  for(int i=0;i<PIN_NUMBER;i++) echoWarning[i] = distanceCmWarning[i]/ECHO_C;
  for(int i=0;i<PIN_NUMBER;i++) connectedPing[i] = true;
  
  Serial.begin(9600);
  t = millis();
}

bool detectAvant = false;
bool detectArriere = false;
bool detectUniqueEtRenvoi = false;

void loop() {
  
    if(Com::readSerial(order)){
      Com::decryptCommand(order,letter,args,argnb);
      switch(letter){
        case 'V':
          detectAvant = args[0] > 0;
          Serial.println("Détection avant : " + String(args[0]));
          break;
        case 'R':
          detectArriere = args[1] > 0;
          Serial.println("Détection arrière : " + String(args[0]));
          break;
        case 'B':
          detectArriere = 1;
          detectUniqueEtRenvoi = true;
      }
    }
  
  if(detectAvant) {
    connectedPing[0] = sonar(PIN_TRIG0,PIN_ECHO0,echoTime[0]);
    connectedPing[1] = sonar(PIN_TRIG1,PIN_ECHO1,echoTime[1]);
    connectedPing[2] = sonar(PIN_TRIG2,PIN_ECHO2,echoTime[2]);
    
          
    if(isObstacleThere(echoTime)){
      Serial.println("Echo times : " + String(echoTime[0]) +" | " +  String(echoTime[1])  +" | " + String(echoTime[2]));
          for(int i=0;i<PIN_NUMBER;i++) echoTime[i] = 0;
      /* trigger a pulse on pin PIN_OBSTACLE to warn motors */
      //trigger(PIN_OBSTACLE);
      //detectAvant = false;
    }
  }
  
  if(detectArriere) {
    connectedPingBack = sonar(PINBACK_TRIG, PINBACK_ECHO,echoTimeBack);
    
    if(detectUniqueEtRenvoi) {
      if(thereIsAGobelet(connectedPingBack,echoTimeBack,echoWarningBack)) 
        Com::writeSerial(StdMsg::ok);
      else 
        Com::writeSerial(StdMsg::no);
      
      //detectArriere = false;
      //detectUniqueEtRenvoi = false;
    }
    else {
      if(isObstacleThere(echoTime)){
          Serial.println("OBSTACLE DERRIEEEEEEEEEEEERE " + String(echoTime[0]));
        /* trigger a pulse on pin PIN_OBSTACLE to warn motors */
        //trigger(PIN_OBSTACLE);
        //detectArriere = false;
      }
    }
  }
}

void trigger(int pin){
  digitalWrite(pin,HIGH);
  delayMicroseconds(10);
  digitalWrite(pin,LOW);
}

bool sonar(int pinTrig, int pinEcho, float &inValue){
  float delta;
  trigger(pinTrig);
  delta = readEcho(pinEcho);
  if(delta != 0){
    delta -= inValue; 
    /* ecretage */
    if(inValue != 0){
      delta = min(delta,abs(inValue)*S_TAUX_MAX);
      delta = max(delta,-abs(inValue)*S_TAUX_MAX);
    }
    /* passe-bas */
    inValue += S_WEIGHT * delta;
    return true;
  } else {
    //ping non connecté
    return false;
  }
}

bool isObstacleThere(float echo[PIN_NUMBER]){
  for(int i=0;i<PIN_NUMBER;i++){
    if(connectedPing[i] && echo[i]<echoWarning[i]) return true;
  }
  return false;
}
      
bool thereIsAGobelet(bool deviceConnected, float echo, float limit){
  if(deviceConnected && echo < limit) return true;
  else return false;
}
