#ifndef LOCALISATION_H_
#define LOCALISATION_H_

class Localisation {
public:
	Localisation(){x = 0; y = 0; angle = 0; countL = 0; countR = 0;};
	float getX(){return x;};
	float getY(){return y;};
	float getAngle(){return angle;};
	void setInitialPosition(float x0,float y0,float a0){x = x0; y = y0; angle = a0;};
	void setInitialCount(int cL, int cR){countL = cL; countR = cR;};
	void updatePosition(int,int);

	float getDistanceTo(float x, float y);
	float getAngleOf(float x, float y);
	float toGeometricAngle(float angle);

	//void display(){ cout << "position : " << x << " " << y << " " << angle << endl;};

private:
		static const float L;      // Diametre du robot (mm)
		static const float K;      // Constante de convertion K * count = cm ie k_rad * R = K
		float x,y,angle;  // x dans l'axe du robot �) l'origine, angle fait par rapport � l'axe x
		int countL,countR;
};

#endif /* LOCALISATION_H_ */
