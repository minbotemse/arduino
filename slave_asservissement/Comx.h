// V2

#include <SoftwareSerial.h>
#include "Arduino.h"

#ifndef STDMESS_H_
#define STDMESS_H_

class StdMsg{
  public :
    static const String ok;
    static const String no;
    static const String pb;
    static String endAction(String);
    static String startAction(String);
};

#endif // STDMESS_H_


#ifndef MPROTO_H_
#define MPROTO_H_

class Com {
public:
	// constructeur
	Com();

	// fonctions de communication
        static bool readSerial(String&);
	static bool readSerial(String&,SoftwareSerial&);
        static void writeSerial(const String&);
	static void writeSerial(const String&,SoftwareSerial&);	
	static void writeBluetooth(const String&);
        static void writeBluetooth(const String&,SoftwareSerial&);
        static bool transferTo(const String&,SoftwareSerial&);

	// fonctions de decryption de commande
	static bool decryptCommand(const String&,char&,float[],int&);
};


#endif // MPROTO_H_



