/*
 * RobotEntity.cpp
 *
 *  Created on: 29 mai 2015
 *      Author: Alexandre
 */

#include "Arduino.h"
#include "RobotEntity.h"

RobotEntity::RobotEntity() {
	state = IDLE;
	tasklist = FIFO<Action>();
	coordinate = Localisation();
	distance.setConsts(0,0,0);
	angle.setConsts(0,0,0);
	sigL = 0;
	sigR = 0;

}

RobotEntity::~RobotEntity() {
        tasklist.clearList();
}

Localisation& RobotEntity::getCoord(){
	return coordinate;
}

void RobotEntity::start(){
	state = IDLE;
}

void RobotEntity::stop(){
	angle.reset();
        distance.reset();
        sigL = 0;
        sigR = 0;
	state = HALTED;
}

void RobotEntity::setFree(){
	stop();
	tasklist.clearList();
}

bool RobotEntity::isTaskComplete(){
	return state == END;
}

float RobotEntity::getSignalL(){
        return sigL;
}

float RobotEntity::getSignalR(){
        return sigR;
}

void RobotEntity::goTo(float x, float y, bool forward){
	Action action1;
	Action action2;
	float alpha;
	alpha = coordinate.getAngleOf(x,y);
	action1 = Action(TURN,x,y,alpha);
	if(forward){
		action2 = Action(STRAIGHT,x,y,alpha);
	} else {
		action2 = Action(BACKWARD,x,y,coordinate.toGeometricAngle(alpha+PI));
	}
	tasklist.add(action1);
	tasklist.add(action2);
}

void RobotEntity::turnTo(float x, float y){
	Action action;
	float alpha;
	alpha = coordinate.getAngleOf(x,y);
	action = {TURN,x,y,alpha};
	tasklist.add(action);
}

void RobotEntity::forwardTo(float d){
        float alpha = coordinate.getAngle();
        float xf = coordinate.getX() + d*cos(alpha);
        float yf = coordinate.getY() + d*sin(alpha);
        Action action;
	if(d>0){
		action = Action(STRAIGHT,xf,yf,alpha);
	} else {
		action = Action(BACKWARD,xf,yf,alpha);
	}
	tasklist.add(action);
}

void RobotEntity::fetchInformation(int cL, int cR){
	STATE nextState;
	// maj de la position du mobile
	coordinate.updatePosition(cL,cR);
	switch(state){
	case BUSY:
                nextState = BUSY;
		float sigAngle, sigDist;
		// maj des variables asservies
		angle.setVar(coordinate.getAngle());
		distance.setVar(coordinate.getDistanceTo(currentAction.x,currentAction.y));
		// calcul des pids
		sigAngle = Pid::getPidValue(angle);
		sigDist = Pid::getPidValue(distance);
		// calcul des signaux action moteur
		switch(currentAction.type){
		case STRAIGHT:
			if((sigDist + abs(sigAngle))<Pid::MAX_SIGNAL){
				sigL = sigDist + sigAngle;
				sigR = sigDist - sigAngle;
			} else {
				// �cr�tage des signaux avec priorit� au signal tourne
				if (sigAngle > 0){
					sigL = Pid::MAX_SIGNAL;
					sigR = Pid::MAX_SIGNAL - 2*sigAngle;
				} else {
					sigL = Pid::MAX_SIGNAL + 2*sigAngle;
					sigR = Pid::MAX_SIGNAL;
				}
			}
			if(distance.isStable()) nextState = IDLE;
			break;
		case BACKWARD:
			if((- sigDist - abs(sigAngle))>Pid::MIN_SIGNAL){
				sigL = - sigDist + sigAngle;
				sigR = - sigDist - sigAngle;
			} else {
				// �cr�tage des signaux avec priorit� au signal tourne
				if (sigAngle < 0){
					sigL = Pid::MIN_SIGNAL;
					sigR = Pid::MIN_SIGNAL - 2*sigAngle;
				} else {
					sigL = Pid::MIN_SIGNAL + 2*sigAngle;
					sigR = Pid::MIN_SIGNAL;
				}
			}
			if(distance.isStable()) nextState = IDLE;
			break;
		case TURN:
			sigL = - sigAngle;
			sigR = sigAngle;
			if(angle.isStable()) nextState = IDLE;
			break;
		default:
			sigL = 0;
			sigR = 0;
			nextState = HALTED;
		}
		break;
	case IDLE:
		if(tasklist.isEmpty()){
			nextState = END;
		} else {
                        nextState = BUSY;
			currentAction = tasklist.pull();
			distance.setReference(0);
			distance.reset();
			angle.setReference(currentAction.angle);
			angle.reset();
			switch(currentAction.type){
			case STRAIGHT:
			case BACKWARD:
				distance.setConsts(40,0,15);
				angle.setConsts(0,10000,0);
				break;
			case TURN:
				distance.setConsts(0,0,0);
				angle.setConsts(200,0,1);
				break;
			default:
				distance.setConsts(0,0,0);
				angle.setConsts(0,0,0);
				break;
			}
		}
		break;
	case END:
	case HALTED :
		nextState = HALTED;
		break;
	default:
		nextState = HALTED;
	}
	state = nextState;
}



