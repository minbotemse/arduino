// V2
#include "Comx.h"

// message standard
const String StdMsg::ok = "OK!";
const String StdMsg::no = "NO!";
const String StdMsg::pb = "PB!";
    
String StdMsg::endAction(String action){
  return String("E:")+action;
}

String StdMsg::startAction(String action){
  return String("S:")+action;
}

// fonctions de communication
bool Com::readSerial(String &trame){
  char lastChar = 'l';
  trame = "";
  
  if(Serial.available()){
    while(lastChar != '!'){
      if(Serial.available()){
        lastChar = (char)Serial.read();
        trame += lastChar;
      } else 
          delay(1);
    }
    return true;
  } else {
    return false;
  }
}

bool Com::readSerial(String &trame, SoftwareSerial &port){
  char lastChar = 'l';
  trame = "";
  
  port.listen();
  if(port.available()){
    while(lastChar != '!'){
      if(port.available()){
        lastChar = (char)port.read();
        trame += lastChar;
      }
      else
        delay(1);
    }
    return true;
  } else {
    return false;
  }
}

void Com::writeSerial(const String &trame){
  int n = trame.length()+1;
  char trameC[n];
  trame.toCharArray(trameC,n);
  Serial.write(trameC);
}

void Com::writeSerial(const String &trame,SoftwareSerial &port){
  int n = trame.length()+1;
  port.listen();
  char trameC[n];
  trame.toCharArray(trameC,n);
  port.write(trameC);
}

void Com::writeBluetooth(const String &trame){
  int n = trame.length();
  for(int i=0;i<n;i++){
      delay(10);
      Serial.print(trame.charAt(i));
  }
  Serial.println();
}

void Com::writeBluetooth(const String &trame,SoftwareSerial &port){
  int n = trame.length();
  port.listen();
  for(int i=0;i<n;i++){
      delay(10);
      port.print(trame.charAt(i));
  }
  port.println();
}

bool Com::transferTo(const String &trame, SoftwareSerial &port){
  String answer;

  Com::writeSerial(trame,port);
  while(!Com::readSerial(answer,port));
  if(answer.equals(StdMsg::pb) || answer.equals(StdMsg::no)){
    return false;
  } else {
    return true;
  }
}


// fonctions de decryption de commande
bool Com::decryptCommand(const String &trame,char &action,float args[],int &argNb){
  int curseur;
  int trameLength;
  char c;
  String numberString;
  
  argNb = 0;
  action = trame.charAt(0);
  
  numberString = "";
  curseur = 2;
  trameLength = trame.length();
  
  c = trame.charAt(curseur);
  while (curseur < trameLength && c != '!') {
    if(c == ';'){
      args[argNb] = numberString.toFloat();
      argNb++;
      numberString = "";
    } else {
      numberString += c;
    }
    curseur++;
    c = trame.charAt(curseur);
  }
  // ajout du dernier nombre
  args[argNb] = numberString.toFloat();
  argNb++;
}
