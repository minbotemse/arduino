/* classe d'asservissement */
#ifndef PID_H
#define PID_H
/*
struct Strategie{
	float KpDistance;
	float KiDistance;
	float KdDistance;
	float KpAngle;
	float KiAngle;
	float KdAngle;
};
 */

class ConstPID{
public:
	ConstPID(){
		Kp = 0;
		Ki = 0;
		Kd = 0;
	};
	ConstPID(float p, float i, float d){
		Kp = p;
		Ki = i;
		Kd = d;
	};
	float Kp;
	float Ki;
	float Kd;
};

class VariablePID{
public :
	VariablePID();
	float getVar(){return u;};
	float getLastInt(){return ui;};
	float getLastDer(){return ud;};
	float getReference(){return ref;};
	float getEps(){return EPS;};
	float getKp(){return Kp;};
	float getKi(){return Ki;};
	float getKd(){return Kd;};
	void setVar(float uf){ud = uf - u; u = uf;};
	void setLastInt(float uif){ui = uif;};
	void setReference(float r){ref = r;};
	void setConsts(float p, float i, float d){Kp = p; Ki = i; Kd = d;};
	void setEps(float e){EPS = e;};
	void setStable(int s){STAB_SEUIL = s;};
	bool isStable(){return (stable>=STAB_SEUIL);};
	void seemStable(){stable++;};
	void seemNotStable(){stable = 0;};
	int getStable(){return stable;};
	void reset();

private :
	float Kp;
	float Ki;
	float Kd;
	float u;
	float ui;
	float ud;
	float ref;
	float EPS;
	int stable;
	int STAB_SEUIL;
};

class Pid{
public :
	/* constantes */
	static const float INT_BOUND;
	static const float MAX_SIGNAL;
	static const float MIN_SIGNAL;

	Pid();
	static float getPidValue(VariablePID&);
	static float cutOff(float,float,float);
};

#endif	





