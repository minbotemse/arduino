#ifndef ROBOTENTITY_H_
#define ROBOTENTITY_H_

#include "FIFO.h"
#include "Localisation.h"
#include "Pid.h"

enum ACTION_TYPE{TURN, STRAIGHT, BACKWARD,NONE};
enum STATE{BUSY, HALTED, IDLE, END};

class Action{
public:
	Action(){
		type = NONE;
		x= 0;
		y = 0;
		angle = 0;
	}

	Action(ACTION_TYPE t, float xf, float yf, float af){
		type = t;
		x= xf;
		y = yf;
		angle = af;
	}
	ACTION_TYPE type;
	float angle;
	float x;
	float y;
};

class RobotEntity {
	// manage the displacement of a moving robot
public:
	RobotEntity();
	virtual ~RobotEntity();
	void start();						// allow the robot to move
	void stop();						// forbid the robot to move
	void setFree();						// clean the task list
	bool isTaskComplete();						// ask if the robot is busy
	void fetchInformation(int,int);		// fetch the informations of the sensors to pursue the current action
	void goTo(float,float,bool);				// plan a straight move to a certain point
	void turnTo(float,float);			// plan to turn toward a certain point
	void forwardTo(float);
	Localisation& getCoord();
	float getSignalL();
	float getSignalR();

private:
	FIFO<Action> tasklist;				// list of all the task to do in order to complete a move
	Localisation coordinate;					// odometry of the robot
	Action currentAction;				// running action
	STATE state;							// information about the state of the robot
	VariablePID angle;
	VariablePID distance;
	float sigL;
	float sigR;
};

#endif /* ROBOTENTITY_H_ */
