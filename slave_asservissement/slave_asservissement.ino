/* PID program */

#include "DualVNH5019MotorShield.h"
#include <SoftwareSerial.h>
#include "RobotEntity.h"
#include "Arduino.h"
#include "Comx.h"

#define T_SAMPLE 50 //ms

/* M2 : right motor
   M1 : left motor */

/* declaration des ports séries */
#define PIN_RXR_MSB 5
#define PIN_RXR_LSB 3
#define PIN_RXL_MSB A3
#define PIN_RXL_LSB A4

/* variables globales */   
DualVNH5019MotorShield MotorShield; 
int cL,cR;
unsigned long t;

String order = "";
char letter;
float args[3];
int argnb;

bool autoMode = true;
bool run = false;
bool sensorReadError = false;

RobotEntity robot;

/* serial declaration */
SoftwareSerial serialR_msb(PIN_RXR_MSB, A2);
SoftwareSerial serialR_lsb(PIN_RXR_LSB, 11);
SoftwareSerial serialL_msb(PIN_RXL_MSB, A5);
SoftwareSerial serialL_lsb(PIN_RXL_LSB, 13);

/* read functions */
int readCL(){
  uint8_t msb;
  uint8_t lsb;
  
  serialL_msb.listen();
  while(!serialL_msb.available());
  msb = serialL_msb.read();
  serialL_lsb.listen();
  while(!serialL_lsb.available());
  lsb = serialL_lsb.read();
  
  return (msb<<8) + lsb;
}

int readCR(){
  uint8_t msb;
  uint8_t lsb;
  
  serialR_msb.listen();
  while(!serialR_msb.available());
  msb = serialR_msb.read();
  serialR_lsb.listen();
  while(!serialR_lsb.available()); 
  lsb = serialR_lsb.read();
  
  return (msb<<8) + lsb;
}

/* motor function */

void motor(int sigL, int sigR){
  MotorShield.setM2Speed(sigL);
  MotorShield.setM1Speed(sigR);
}

void setup() {  
  /* initialisation des pins */
  pinMode(PIN_RXR_MSB,INPUT);
  pinMode(PIN_RXR_LSB,INPUT);
  //pinMode(A2,OUTPUT);
  //pinMode(A4,OUTPUT);
  pinMode(PIN_RXL_MSB,INPUT);
  pinMode(PIN_RXL_LSB,INPUT);
  //pinMode(A5,OUTPUT);
  pinMode(11,OUTPUT);
  
  
  /** initialisation des ports **/
  serialR_msb.begin(9600);
  serialR_lsb.begin(9600);
  serialL_msb.begin(9600);
  serialL_lsb.begin(9600);
  Serial.begin(9600);
  /******************************/
  
  MotorShield.init();
  t = millis();
}



void loop() {
    cL = readCL();
    cR = readCR();
    if(!run && Com::readSerial(order)){
        Com::decryptCommand(order,letter,args,argnb);
        switch(letter){
        case 'W':
            autoMode = true;
            Com::writeSerial(StdMsg::ok);
            // Serial.print(autoMode);
            robot.setFree();
            break;
        case 'M':
            autoMode = false;
            Com::writeSerial(StdMsg::ok);
            // Serial.print(autoMode);
            robot.setFree();
            break;
        case 'C':
            if(argnb == 2){
                robot.goTo(args[0],args[1],true);
                run = true;
                robot.start();
                Com::writeSerial(StdMsg::startAction(order));
            } else {
                Com::writeSerial(StdMsg::pb);
            }
            break;
        case 'O':
            if(argnb == 2){
                robot.turnTo(args[0],args[1]);
                run = true;
                robot.start();
                Com::writeSerial(StdMsg::startAction(order));
            } else {
                Com::writeSerial(StdMsg::pb);
            }
            break;
        case 'A':
            if(argnb == 1){
                robot.forwardTo(args[0]);
                run = true;
                robot.start();
                Com::writeSerial(StdMsg::startAction(order));
            } else {
                Com::writeSerial(StdMsg::pb);
            }
            break;
        }
    }
    if(run && ((millis()-t) > T_SAMPLE)){
        t = millis();
        robot.fetchInformation(cL,cR);
        motor(robot.getSignalL(),robot.getSignalR());
        if(robot.isTaskComplete()){
            motor(0,0);
            run = false;
            Com::writeSerial(StdMsg::endAction(order));
        }
    }
}

