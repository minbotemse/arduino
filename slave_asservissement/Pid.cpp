#define SEUIL 10
#define SEUIL_MAX_MOTEUR 300
#define SEUIL_MIN_MOTEUR 50

#include "Arduino.h"
#include "Pid.h"

/*
const float Pid::EPS_D = 10;
const float Pid::EPS_A = 0.02;
const float Pid::MAX_COUNT_DIFF = 200;
 */
const float Pid::INT_BOUND = 100;
const float Pid::MIN_SIGNAL = -300;
const float Pid::MAX_SIGNAL = 300;

VariablePID::VariablePID(){
	Kp = 0;
	Ki = 0;
	Kd = 0;
	u = 0;
	ui = 0;
	ud = 0;
	ref = 0;
	EPS = 1;
	stable = 0;
	STAB_SEUIL = 5;
}

void VariablePID::reset(){
	ui = 0;
	ud = 0;
}

float Pid::getPidValue(VariablePID &var){
	float e,ei,ed;
	float pidValue;
	e = var.getVar() - var.getReference();
	if(abs(e) > var.getEps()){
		var.seemNotStable();
		// régulation des petites variations
		ei = Pid::cutOff(var.getLastInt() + e,-Pid::INT_BOUND,Pid::INT_BOUND);
		var.setLastInt(ei);
		// accentuation de la décélération/accélération
		ed = var.getLastDer();
		pidValue = Pid::cutOff(var.getKp()*e + var.getKi()*ei + var.getKd()*ed,Pid::MIN_SIGNAL,Pid::MAX_SIGNAL);
	} else {
		var.seemStable();
		pidValue = 0;
	}
	return pidValue;
}

float Pid::cutOff(float a, float min, float max){
	if(a<min) return min;
	else if (a>max) return max;
	else return a;
}

