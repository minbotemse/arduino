#include "Arduino.h"
#include "Localisation.h"

const float Localisation::L = 24;
const float Localisation::K = 0.23;

void Localisation::updatePosition(int cL,int cR){
	float distanceParcourue;
	distanceParcourue = K / 2 * (cL - countL + cR - countR);
	x += distanceParcourue * cos(angle);
	y += distanceParcourue * sin(angle);
	angle = toGeometricAngle(angle + K / L * (cR - countR - cL + countL));
	countL = cL;
	countR = cR;
}

float Localisation::getDistanceTo(float xf, float yf){
	return sqrt(pow(x-xf,2) + pow(y-yf,2));
}

float Localisation::getAngleOf(float xf, float yf){
	return yf - y > 0 ? acos((xf-x)/getDistanceTo(xf,yf)) : -acos((xf-x)/getDistanceTo(xf,yf));
}

float Localisation::toGeometricAngle(float angle){
	while(angle > PI) angle -= 2*PI;
	while(angle < -PI) angle += 2*PI;
	return angle;
}
