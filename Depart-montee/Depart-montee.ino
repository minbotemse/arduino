#include <Servo.h>


// functional connections
#define MOTOR_B_DIR 5 // Motor B Direction
#define MOTOR_A_DIR 4// Motor A Direction
//Boolens
int inclinaison = 0; //0 si le robot est non incliné
int escalier=0;
int dessusescaliers = 0;
// temps et longueurs
int tempsrotation;
long cm;
int tempsdebut;
int tempsLigneDroite1 = 5500; //ex 5000
//ping
int trig = 7;
int echo = 6;
int lecture_echo;
//conditions initiales
int departdroit = 10;
int departgauche = 11;
// autres pin
int captinclin = 8; 
int fil = 12;
int led = 13; 
int tempsservo = 940; 
Servo Servo1;
int pinServo1 = 3;
Servo Servo2;
int pinServo2 = 9;

//interruption du programme
void interruption(){
digitalWrite(led, LOW);
digitalWrite( MOTOR_A_DIR, LOW ); // direction = forward
digitalWrite( MOTOR_B_DIR, LOW ); // direction = forward
while(1){
delay(10000);}
}

//calcul de la distance en cm
long calcul_distance(){
  Serial.println("Je commence a calculer la distance");
          digitalWrite(trig, HIGH);
          delayMicroseconds(10);
          digitalWrite(trig, LOW);
          lecture_echo=pulseIn(echo, HIGH);
          cm = lecture_echo / 58;
return cm;
}

void setup()
{
  //moteurs
  pinMode( MOTOR_B_DIR, OUTPUT );
  pinMode( MOTOR_A_DIR, OUTPUT );
  digitalWrite( MOTOR_B_DIR, LOW );
  digitalWrite( MOTOR_A_DIR, LOW );
  //led
  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);
  Serial.begin(9600);
  //interrupteur de depart
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(0,interruption,CHANGE);
  pinMode(fil,INPUT_PULLUP);
  //ping
  pinMode(trig, OUTPUT);
  digitalWrite(trig, LOW);
  pinMode(echo, INPUT);
  digitalWrite(echo,LOW);
  //detecteur d'inclinaison
  pinMode(captinclin,INPUT_PULLUP);
  //choix des conditions intiales
  pinMode(10, INPUT_PULLUP); //depart a droite
  pinMode(11, INPUT_PULLUP); //depart a gauche

  Servo1.attach(pinServo1);
  Servo1.writeMicroseconds(tempsservo);
  //Servo2.attach(pinServo2);
  //Servo2.writeMicroseconds(tempsservo);
}

void loop() {
  
  if(digitalRead(fil) == HIGH){
    //initialisation du temps
    tempsdebut = millis();
    
    if(digitalRead(departgauche) == HIGH){
      tempsrotation = 1900;
    }
      
    if(digitalRead(departdroit) == HIGH){
      tempsrotation = 1500;
    }
    
    if(dessusescaliers == 0){
      if(escalier == 0){
        //Controle du robot avant la montee  
          // Premiere ligne droie
          while(millis()-tempsdebut < tempsLigneDroite1){
            digitalWrite( MOTOR_A_DIR, HIGH ); // direction = forward
            digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
            //Detection d'un obstacle
            Serial.println("J'avance en ligne droite");
            cm = calcul_distance();
            
            //En cas de détection
            while(cm < 5){
              Serial.println("Je vois un obstacle");
              //arret temporaire des moteurs
              digitalWrite( MOTOR_B_DIR, LOW );
              digitalWrite( MOTOR_A_DIR, LOW );
              delay(200);
              cm = calcul_distance();
              tempsLigneDroite1 = tempsLigneDroite1 + 200; 
            }
          }
          
          //Rotation du robot
          if(digitalRead(departgauche) == HIGH){
            digitalWrite( MOTOR_A_DIR, LOW );}
          if(digitalRead(departdroit) == HIGH){
            digitalWrite( MOTOR_B_DIR, LOW );}
            while(millis()-tempsdebut < tempsLigneDroite1 + tempsrotation){
            }
     
          // Deuxieme ligne droie
          while(millis() - tempsdebut < tempsLigneDroite1 + tempsrotation + 8000){
            digitalWrite( MOTOR_A_DIR, HIGH ); // direction = forward
            digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
          //Detection d'un obstacle
            cm = calcul_distance();
            
            //En cas de détection
            while(cm<5){
              digitalWrite( MOTOR_B_DIR, LOW );
              digitalWrite( MOTOR_A_DIR, LOW );
              delay(200);
              cm = calcul_distance();
              tempsLigneDroite1 = tempsLigneDroite1 + 200;
              } 
          }
  
         // On détecte la première inclinaison
          
          escalier = 1;
        }
          
        digitalWrite( MOTOR_A_DIR, HIGH ); // direction = forward
        digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
          
        if(digitalRead(captinclin) == LOW){
          Servo1.writeMicroseconds(740);
          //if(digitalRead(captinclin)== LOW){  
            digitalWrite(led,LOW);
            dessusescaliers = 1;
            Serial.println("Au dessus des marches");
            delay(800);
            //arret des moteurs
            digitalWrite( MOTOR_A_DIR, LOW ); 
            digitalWrite( MOTOR_B_DIR, LOW );
          //}
          
        }//Fin de la montee des marches
    } // Fin du code avant d'etre arrive au dessus des marches
      
    //Au dessus des marches
    else{
  
    }
          
      if(millis() - tempsdebut > 90000){
        Serial.println("Limite de temps depassee ");
        //arret des moteurs
        digitalWrite( MOTOR_A_DIR, LOW ); 
        digitalWrite( MOTOR_B_DIR, LOW ); 
        while(1){
          delay(5000);
        }
      }
  }    
}
