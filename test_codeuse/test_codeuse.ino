/* TESTBENCH : codeuse */

#include <SoftwareSerial.h>
#include "Arduino.h"

#define T_SAMPLE 10 //ms

/* declaration des ports séries */
#define PIN_RXR_MSB 5
#define PIN_RXR_LSB 3
#define PIN_RXL_MSB A3
#define PIN_RXL_LSB A4

/* variables globales */   

int cR;
int cL;
unsigned long t;
  
/* serial declaration */
SoftwareSerial serialR_msb(PIN_RXR_MSB, A2);
SoftwareSerial serialR_lsb(PIN_RXR_LSB, 11);
SoftwareSerial serialL_msb(PIN_RXL_MSB, A5);
SoftwareSerial serialL_lsb(PIN_RXL_LSB, 13);

/* read functions */
int readCL(){
  uint8_t msb;
  uint8_t lsb;
  
  serialL_msb.listen();
  while(!serialL_msb.available());
  msb = serialL_msb.read();
  serialL_lsb.listen();
  while(!serialL_lsb.available());
  lsb = serialL_lsb.read();
  
  return (msb<<8) + lsb;
}

int readCR(){
  uint8_t msb;
  uint8_t lsb;
  
  serialR_msb.listen();
  while(!serialR_msb.available());
  msb = serialR_msb.read();
  serialR_lsb.listen();
  while(!serialR_lsb.available());
  lsb = serialR_lsb.read();
  
  return (msb<<8) + lsb;
}

void setup() {  
  /* initialisation des pins */
  pinMode(PIN_RXR_MSB,INPUT);
  pinMode(PIN_RXR_LSB,INPUT);
  //pinMode(A2,OUTPUT);
  //pinMode(A4,OUTPUT);
  pinMode(PIN_RXL_MSB,INPUT);
  pinMode(PIN_RXL_LSB,INPUT);
  //pinMode(A5,OUTPUT);
  pinMode(11,OUTPUT);
  
  
  /** initialisation des ports **/
  serialR_msb.begin(9600);
  serialR_lsb.begin(9600);
  serialL_msb.begin(9600);
  serialL_lsb.begin(9600);
  Serial.begin(9600);
  /******************************/
  

  t = millis();
}



void loop() {
  
  if((millis()-t) > T_SAMPLE){
    t = millis();
    cL = readCL();
    cR = readCR();
    /*
    pid->updateEncoderCount(cL,cR);
    pid->updatePosition();
    pid->updatePid();
    */
    Serial.print(cL);Serial.print(" - ");Serial.println(cR);
  } 
}

